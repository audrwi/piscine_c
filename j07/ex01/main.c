/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/08 15:33:46 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/08 23:09:51 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		*ft_range (int min, int max);

int		main ()
{
	int index;
	int *tab;
	int min = 1;
	int max = 3;

	tab = ft_range(min, max);
	while (index < max)
	{
		printf("%d", tab[index]);
		index++;
	}
	return (0);
}
