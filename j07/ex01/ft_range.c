/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/08 15:33:07 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/08 23:13:10 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range (int min, int max)
{
	int index;
	int *tab;

	if (min >= max)
	{
		return (0);
	}
	else
	{
		tab = malloc(sizeof(max - min));
		index = 0;
		while (index < (max - min))
		{
			tab[index] = min + index;
			index++;
		}
		return (tab);
	}
}
