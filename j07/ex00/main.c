/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/08 14:19:50 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/08 14:22:41 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strdup (char *src);

int		main ()
{
	char src[] = "hodor";

	printf("%s / %p, %s / %p", src, src, ft_strdup(src), ft_strdup(src));
	return (0);
}
