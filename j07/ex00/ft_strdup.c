/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/07 21:50:12 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/08 14:22:09 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup (char *src)
{
	int index;
	char *str;

	if (src[0] == '\0')
	{
		return (NULL);
	}
	index = 0;
	while (src[index] != '\0')
	{
		str = malloc(sizeof(src + 1));
		index++;
	}
	index = 0;
	while (src[index] != '\0')
	{
		str[index] = src[index];
		index++;
	}
	str[index] = '\0';
	return (str);
}
