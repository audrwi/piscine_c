/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/05 18:28:51 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/06 15:52:31 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_iterative_power (int nb, int power);

int		main()
{
	printf("%d%s", ft_iterative_power(5, 2), "\n");
	return (0);
}
