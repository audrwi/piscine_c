/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/05 18:16:07 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/05 19:30:25 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int ft_iterative_power (int nb, int power)
{
	int n;

	n = nb;
	if (power > 0)
	{
		while (power > 1)
		{
			nb = nb * n;
			power--;
		}
		return (nb);
	}
	else if (power == 0)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}
