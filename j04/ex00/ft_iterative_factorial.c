/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/05 13:56:01 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/05 18:18:12 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial (int nb)
{
	int n;

	if (nb > 0 && nb <= 12)
	{
		n = 1;
		while (nb > 0)
		{
			n = n * nb;
			nb--;
		}
		return (n);
	}
	else if (nb == 0)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}
