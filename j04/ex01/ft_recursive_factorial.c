/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/05 14:16:26 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/05 22:58:39 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_recursive_factorial (int nb)
{
	int n;

	n = nb;
	if (nb > 0 && nb <= 12)
	{
		if (nb > 1)
		{
			n = n * ft_recursive_factorial(nb - 1);
		}
		return (n);
	}
	else if (nb == 0)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}
