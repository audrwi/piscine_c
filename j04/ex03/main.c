/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/05 21:24:21 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/06 15:54:11 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_recursive_power (int nb, int power);

int		main(int n)
{
	printf("%d%s", ft_recursive_power(5, 2), "\n");
	return (0);
}
