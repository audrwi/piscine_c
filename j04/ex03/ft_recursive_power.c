/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/05 21:18:24 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/05 21:46:22 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_recursive_power (int nb, int power)
{
	if (power > 0)
	{
		if (power > 1)
		{
			nb = nb * ft_recursive_power(nb, --power);
		}
		return (nb);
	}
	else if (power == 0)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}
