/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle-01.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: chlucet <chlucet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/02 14:50:37 by chlucet           #+#    #+#             */
/*   Updated: 2014/08/02 19:45:20 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);
int		main(void);

void	lines(int x, char a, char b, char c)
{
	int column;

	column = 0;
	while (column < x)
	{
		if (column == 0)
			ft_putchar(a);
		else if (column < (x - 1))
			ft_putchar(b);
		else
			ft_putchar(c);
		column++;
	}
}

void	colle(int x, int y)
{
	int	line;

	line = 0;
	while (line < y)
	{
		if (line == 0)
			lines(x, 'A', 'B', 'A');
		else if (line < (y - 1))
			lines(x, 'B', ' ', 'B');
		else
			lines(x, 'C', 'B', 'C');
		ft_putchar('\n');
		line++;
	}
}
