/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lfouquet <lfouquet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/10 16:18:01 by lfouquet          #+#    #+#             */
/*   Updated: 2014/08/10 16:26:51 by lfouquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		main(int argc, char **argv)
{
	t_loc	loc;
	char	*cpy[9];
	int		i;

	loc.row = 0;
	loc.col = 0;
	argv = argv + 1;
	i = 0;
	while (i < 9)
		cpy[i++] = (char*)malloc(sizeof(char) * 9);
	*cpy = (char*)malloc(sizeof(char) * 9);
	ft_strtabcpy(argv, cpy);
	if (ft_check_structure(argc, argv) && ft_resolve_sudoku(argv, loc, '1'))
	{
		ft_resolve_sudoku_reverse(cpy, loc, '9');
		if (ft_strtabcmp(cpy, argv))
			ft_print_sudoku(cpy);
		else
			ft_putstr("Erreur\n");
	}
	else
		ft_putstr("Erreur\n");
	return (0);
}
