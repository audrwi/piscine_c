/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolve_sudoku.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lfouquet <lfouquet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/10 15:21:34 by lfouquet          #+#    #+#             */
/*   Updated: 2014/08/10 16:29:09 by lfouquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		ft_resolve_sudoku(char **sudoku, t_loc location, char number)
{
	int		result;

	if (location.row < 9 && location.col < 9)
	{
		if (sudoku[location.row][location.col] != '.')
			return (ft_next_case(sudoku, location, number));
		else
		{
			while (number <= '9')
			{
				result = ft_attribute_value(sudoku, location, number);
				if (result == 1)
					return (result);
				number++;
			}
		}
		return (0);
	}
	else
		return (1);
}

int		ft_next_case(char **sudoku, t_loc location, char number)
{
	if (location.col + 1 < 9)
	{
		location.col++;
		return (ft_resolve_sudoku(sudoku, location, number));
	}
	else if (location.row + 1 < 9)
	{
		location.row++;
		location.col = 0;
		return (ft_resolve_sudoku(sudoku, location, number));
	}
	else
		return (1);
}

int		ft_attribute_value(char **sudoku, t_loc location, char number)
{
	if (ft_check_row(number, location, sudoku)
		&& ft_check_colum(number, location, sudoku)
		&& ft_check_block(number, location, sudoku))
	{
		sudoku[location.row][location.col] = number;
		if (location.row == 8 && location.col == 8)
			return (1);
		else if (ft_next_case(sudoku, location, '1'))
			return (1);
		else
			sudoku[location.row][location.col] = '.';
	}
	return (0);
}
