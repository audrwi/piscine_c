/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sodoku.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lfouquet <lfouquet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/10 16:12:16 by lfouquet          #+#    #+#             */
/*   Updated: 2014/08/10 19:02:23 by lfouquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"
#include <stdio.h>

int			ft_check_row(char number, t_loc location, char **sudoku)
{
	int		i;

	i = 0;
	while (i < 9)
	{
		if (sudoku[location.row][i] == number && number != 46
			&& i != location.col)
			return (0);
		i++;
	}
	return (1);
}

int			ft_check_colum(char number, t_loc location, char **sudoku)
{
	int		i;

	i = 0;
	while (i < 9)
	{
		if (sudoku[i][location.col] == number && number != 46
			&& location.row != i)
			return (0);
		i++;
	}
	return (1);
}

int			ft_check_block(char number, t_loc location, char **sudoku)
{
	int		row;
	int		col;
	int		i;
	int		j;

	row = location.row / 3 * 3;
	col = location.col / 3 * 3;
	i = 0;
	while (i < 3)
	{
		j = 0;
		while (j < 3)
		{
			if (sudoku[row + i][col + j] == number && number != 46
				&& location.row != (row + i) && location.col != (col + j))
			{
				return (0);
			}
			j++;
		}
		i++;
	}
	return (1);
}

int			ft_check_multi_charact(int nb_params, char **sudoku)
{
	int		i;
	int		j;
	t_loc	loc;

	i = 0;
	loc.row = 0;
	loc.col = 0;
	while (i < nb_params - 1)
	{
		j = 0;
		while (sudoku[i][j])
		{
			loc.row = i;
			loc.col = j;
			if (ft_check_block(sudoku[i][j], loc, sudoku) == 0)
				return (0);
			if (ft_check_row(sudoku[i][j], loc, sudoku) == 0)
				return (0);
			if (ft_check_colum(sudoku[i][j], loc, sudoku) == 0)
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int			ft_check_structure(int nb_params, char **sudoku)
{
	int		i;
	int		j;

	if (nb_params != 10)
		return (0);
	i = 0;
	while (i < nb_params - 1)
	{
		j = 0;
		while (sudoku[i][j] != '\0')
		{
			if (!((sudoku[i][j] >= '1' && sudoku[i][j] <= '9')
					|| sudoku[i][j] == '.'))
				return (0);
			j++;
		}
		if (j != 9)
			return (0);
		i++;
	}
	if (ft_check_multi_charact(nb_params, sudoku) == 0)
		return (0);
	return (1);
}
