/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolve_sudoku_reverse.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lfouquet <lfouquet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/10 16:22:54 by lfouquet          #+#    #+#             */
/*   Updated: 2014/08/10 16:29:14 by lfouquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		ft_resolve_sudoku_reverse(char **sudoku, t_loc location, char number)
{
	int		result;

	if (location.row < 9 && location.col < 9)
	{
		if (sudoku[location.row][location.col] != '.')
			return (ft_next_case_reverse(sudoku, location, number));
		else
		{
			while (number >= '1')
			{
				result = ft_attribute_value_reverse(sudoku, location, number);
				if (result == 1)
					return (result);
				number--;
			}
		}
		return (0);
	}
	else
		return (1);
}

int		ft_next_case_reverse(char **sudoku, t_loc location, char number)
{
	if (location.col + 1 < 9)
	{
		location.col++;
		return (ft_resolve_sudoku_reverse(sudoku, location, number));
	}
	else if (location.row + 1 < 9)
	{
		location.row++;
		location.col = 0;
		return (ft_resolve_sudoku_reverse(sudoku, location, number));
	}
	else
		return (1);
}

int		ft_attribute_value_reverse(char **sudoku, t_loc location, char number)
{
	if (ft_check_row(number, location, sudoku)
		&& ft_check_colum(number, location, sudoku)
		&& ft_check_block(number, location, sudoku))
	{
		sudoku[location.row][location.col] = number;
		if (location.row == 8 && location.col == 8)
			return (1);
		else if (ft_next_case_reverse(sudoku, location, '9'))
			return (1);
		else
			sudoku[location.row][location.col] = '.';
	}
	return (0);
}
