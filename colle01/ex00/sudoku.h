/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lfouquet <lfouquet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/10 15:19:27 by lfouquet          #+#    #+#             */
/*   Updated: 2014/08/10 19:13:31 by lfouquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUDOKU_H
# define SUDOKU_H

# include <unistd.h>
# include <stdlib.h>

typedef struct	s_loc
{
	int			row;
	int			col;
}				t_loc;

/*
** sudoku.c
*/
int				ft_check_row(char number, t_loc location, char **sudoku);
int				ft_check_colum(char number, t_loc location, char **sudoku);
int				ft_check_block(char number, t_loc location, char **sudoku);
int				ft_check_structure(int nb_params, char **sudoku);

/*
** fonction.c
*/
void			ft_putstr(char *str);
void			ft_putchar(char c);
void			ft_strtabcpy(char **src, char **dest);
int				ft_strtabcmp(char **tab1, char **tab2);
void			ft_print_sudoku(char **sudoku);

/*
** resolve_sudoku.c
*/
int				ft_resolve_sudoku(char **sudoku, t_loc location, char number);
int				ft_next_case(char **sudoku, t_loc location, char number);
int				ft_attribute_value(char **sudoku, t_loc location, char number);

/*
** resolve_sudoku_reverse.c
*/
int				ft_resolve_sudoku_reverse(char **sudoku, t_loc location
	, char number);
int				ft_next_case_reverse(char **sudoku, t_loc location,
	char number);
int				ft_attribute_value_reverse(char **sudoku, t_loc location,
	char number);

#endif
