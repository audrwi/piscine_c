/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fonction.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lfouquet <lfouquet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/10 15:51:51 by lfouquet          #+#    #+#             */
/*   Updated: 2014/08/10 17:09:43 by lfouquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

void		ft_putstr(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void		ft_putchar(char c)
{
	write(1, &c, 1);
}

void		ft_strtabcpy(char **src, char **dest)
{
	int	i;
	int	j;

	i = 0;
	while (src[i] != 0)
	{
		j = 0;
		while (src[i][j] != 0)
		{
			dest[i][j] = src[i][j];
			j++;
		}
		i++;
	}
}

int			ft_strtabcmp(char **tab1, char **tab2)
{
	int		i;
	int		j;
	int		rslt;

	i = 0;
	rslt = 1;
	while (tab2[i] != 0)
	{
		j = 0;
		while (tab2[i][j] != 0)
		{
			if (tab1[i][j] != tab2[i][j])
				rslt = 0;
			j++;
		}
		i++;
	}
	return (rslt);
}

void		ft_print_sudoku(char **sudoku)
{
	int		i;
	int		j;

	i = 0;
	while (i < 9)
	{
		j = 0;
		while (sudoku[i][j] != 0)
		{
			ft_putchar(sudoku[i][j]);
			if (j != 8)
				ft_putchar(' ');
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}
