/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inter.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/23 12:51:23 by exam              #+#    #+#             */
/*   Updated: 2014/08/23 13:30:44 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_while(char *s1, char *s2, int i)
{
	int		k;

	k = 0;
	while (s2[k] != '\0')
	{
		if (s1[i] == s2[k])
		{
			ft_putchar(s1[i]);
			while (s2[k] != '\0')
				k++;
		}
		else
			k++;
	}
}

void	ft_inter(char *s1, char *s2)
{
	int		i;
	int		j;

	i = 0;
	while (s1[i] != '\0')
	{
		j = i - 1;
		while (j >= 0)
		{
			if (s1[i] == s1[j])
				j = -50;
			else
				j--;
		}
		if (j != -50)
			ft_while(s1, s2, i);
		i++;
	}
}

int		main(int argc, char **argv)
{
	if (argc == 3)
		ft_inter(argv[1], argv[2]);
	ft_putchar('\n');
	return (0);
}
