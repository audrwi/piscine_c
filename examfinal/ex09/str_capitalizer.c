/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_capitalizer.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/23 11:11:40 by exam              #+#    #+#             */
/*   Updated: 2014/08/23 11:38:04 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_str_capitalizer(char *str, int i)
{
	if (str[0] >= 'a' && str[0] <= 'z')
		ft_putchar(str[0] - 32);
	else
		ft_putchar(str[0]);
	while (str[i] != '\0')
	{
		if (str[i] == ' ' || str[i] == '	')
		{
			while (str[i] == ' ' || str[i] == '	')
			{
				ft_putchar(str[i]);
				i++;
			}
			if (str[i] >= 'a' && str[i] <= 'z')
				ft_putchar(str[i] - 32);
			else
				ft_putchar(str[i]);
		}
		else if (str[i] >= 'A' && str[i] <= 'Z')
			ft_putchar(str[i] + 32);
		else
			ft_putchar(str[i]);
		i++;
	}
}

int		main(int argc, char **argv)
{
	int		i;

	i = 1;
	if (argc >= 2)
	{
		while (i < argc)
		{
			ft_str_capitalizer(argv[i], 1);
			i++;
		}
	}
	ft_putchar('\n');
	return (0);
}
