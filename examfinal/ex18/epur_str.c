/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   epur_str.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/23 13:32:18 by exam              #+#    #+#             */
/*   Updated: 2014/08/23 14:05:13 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_epur_str(char *str, int i, int j)
{
	while (str[j] != '\0')
		j++;
	j--;
	if (str[j] == ' ' || str[j] == '	')
	{
		while (str[j] == ' ' && str[j] == '	')
			j--;
	}
	if (str[i] == ' ' || str[i] == '	')
		while (str[i] == ' ' || str[i] == '	')
			i++;
	while (str[i] != '\0')
	{
		if ((str[i] == ' ' || str[i] == '	') && (i != j && i != 0))
		{
			while (str[i] == ' ' || str[i] == '	')
				i++;
			ft_putchar(' ');
		}
		ft_putchar(str[i]);
		i++;
	}
}

int		main(int argc, char **argv)
{
	if (argc == 2)
		ft_epur_str(argv[1], 0, 0);
	ft_putchar('\n');
	return (0);
}
