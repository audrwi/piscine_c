/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/15 10:18:37 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/15 12:02:26 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

unsigned int	ft_collatz_conjecture(unsigned int base);

int				main(void)
{
	printf("%d%s", ft_collatz_conjecture(1), " ");
	printf("%d%s", ft_collatz_conjecture(2), " ");
	printf("%d%s", ft_collatz_conjecture(3), " ");
	printf("%d%s", ft_collatz_conjecture(4), " ");
	printf("%d%s", ft_collatz_conjecture(5), " ");
	printf("%d%s", ft_collatz_conjecture(6), " ");
	return (0);
}
