/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_spy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/15 12:13:18 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/15 15:32:17 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_strcmp(char *s1, char *s2)
{
	int		i;

	i = 0;
	while (s2[i] != '\0')
	{
		if (s1[i] == s2[i])
			i++;
		else
			return (0);
	}
	while (s1[i] != ' ' && s1[i] != '\0')
		return (0);
	return (1);
}

int		ft_spy(char *str)
{
	int		i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] >= 65 && str[i] <= 90)
			str[i] = str[i] + 32;
		i++;
	}
	i = 0;
	while (str[i] == ' ')
		i++;
	if (ft_strcmp(str + i, "president") == 1)
		return (1);
	else if (ft_strcmp(str + i, "attack") == 1)
		return (1);
	else if (ft_strcmp(str + i, "powers") == 1)
		return (1);
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;

	i = 2;
	if (argc >= 2)
	{
		while (i <= argc)
		{
			if (ft_spy(argv[i - 1]) == 1)
			{
				write(1, "Alert!!!\n", 10);
				return (0);
			}
			else
				i++;
		}
	}
	return (0);
}
