/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rot42.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/15 00:26:11 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/15 14:13:07 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_while(char *str, char a, char z, int i)
{
	int		count;

	count = 0;
	while (count < 42)
	{
		if (str[i] != z)
			str[i]++;
		else
			str[i] = a;
		count++;
	}
}

char	*ft_rot42(char *str)
{
	int		i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] >= 'a' && str[i] <= 'z')
			ft_while(str, 'a', 'z', i);
		else if (str[i] >= 'A' && str[i] <= 'Z')
			ft_while(str, 'A', 'Z', i);
		i++;
	}
	return (str);
}
