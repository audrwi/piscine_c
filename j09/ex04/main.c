/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/15 00:41:18 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/15 00:44:43 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_rot42(char *str);

int		main(int argc, char **argv)
{
	if (argc == 2)
		printf("%s", ft_rot42(argv[1]));
	return (0);
}
