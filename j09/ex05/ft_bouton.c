/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bouton.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/15 09:29:55 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/15 09:55:27 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_bouton(int i, int j, int k)
{
	if ((i < j && j < k) || (i > j && j > k))
		return (j);
	else if ((j < i && i < k) || (j > i && i > k))
		return (i);
	else if ((i < k && k < j) || (i > k && k > j))
		return (k);
	else if ((i == j && j != k) || (j == k && j != i))
		return (j);
	else if (i == k && i != j)
		return (i);
	return (j);
}
