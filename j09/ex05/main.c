/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/15 09:30:50 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/15 09:55:02 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_bouton(int i, int j, int k);

int		main(void)
{
	printf("%d%s", ft_bouton(1,2,3), "\n");
	printf("%d%s", ft_bouton(2,1,3), "\n");
	printf("%d%s", ft_bouton(3,2,1), "\n");
	printf("%d%s", ft_bouton(1,1,1), "\n");
	printf("%d%s", ft_bouton(1,1,2), "\n");
	printf("%d%s", ft_bouton(1,2,1), "\n");
	printf("%d%s", ft_bouton(2,1,1), "\n");
	return (0);
}
