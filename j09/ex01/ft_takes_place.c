/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_takes_place.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/14 19:48:05 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/15 14:00:34 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_takes_place(int	hour)
{
	printf("%s", "THE FOLLOWING TAKES PLACE BETWEEN ");
	if (hour == 0 || hour == 24)
		printf("%s", "12 A.M. AND 1 A.M.\n");
	else if (hour == 23)
		printf("%s", "11 P.M. AND 12 A.M.\n");
	else if (hour > 12)
		printf("%d%s%d%s", hour - 12, " P.M. AND ", hour - 11, " P.M.\n");
	else if (hour == 11)
		printf("%s", "11 A.M. AND 12 P.M.\n");
	else if (hour == 12)
		printf("%s", "12 P.M. AND 1 P.M.\n");
	else
		printf("%d%s%d%s", hour, " A.M. AND ", hour + 1, " A.M.\n");
}
