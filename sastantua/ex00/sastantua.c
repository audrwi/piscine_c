/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sastantua.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/03 10:21:07 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/03 22:52:31 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar (char c)
{
	write(1, &c, 1);
	return (0);
}

void	sastantua (int size)
{
	int step;
	int line;
	int star;
	int nb_star;

	step = 1;
	line = 0;
	star = 1;
	nb_star = 1;
	while (step <= size)
	{
		ft_putchar('/');
		while (star <= nb_star + 2 * line + 6 * (step - 1))
		{
			ft_putchar('*');
			star++;
		}
		nb_star = star;
		ft_putchar('\\');
		ft_putchar('\n');
		line++;
		if (line == (step + 2))
		{
			step++;
			line = 0;
		}
	}
}

int		main (void)
{
	sastantua(5);
	return (0);
}
