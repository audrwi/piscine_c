/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/07 10:34:50 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/07 11:08:54 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp (char *s1, char *s2)
{
	int index;

	index = 0;
	while (s1[index] == s2[index])
	{
		if (s1[index] == '\0' && s2[index] == '\0')
		{
			return (0);
		}
		index++;
	}
	return (s1[index] - s2[index]);
}
