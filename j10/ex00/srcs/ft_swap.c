/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/07 10:53:37 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/07 10:54:31 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_swap (int *a, int *b)
{
	int c;

	c = *a;
	*a = *b;
	*b = c;
}
