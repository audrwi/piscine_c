/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/10 15:04:26 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/10 15:27:15 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putstr(char *str);
int		check_sudoku(int argc, char **argv);

int		main(int argc, char **argv)
{
	if (check_sudoku(argc, argv + 1) == 0)
		ft_putstr("Erreur\n");
	return (0);
}
