/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_sudoku.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/09 17:15:26 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/10 18:44:10 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		check_line(int argc, char **argv, int i, int j)
{
	int		k;

	while (i < argc - 1)
	{
		while (argv[i][j])
		{
			if (argv[i][j] != 46)
			{
				k = j + 1;
				while (argv[i])
				{
					if (argv[i][j] == argv[i][k])
						return (0);
					k++;
				}
			}
			j++;
		}
		j = 0;
		i++;
	}
	return (1);
}

int		check_col(int argc, char **argv, int i, int j)
{
	int		k;

	while (argv[i][j])
	{
		while (i < argc - 1)
		{
			if (argv[i][j] != 46)
			{
				k = i + 1;
				while (argv[i])
				{
					if (argv[i][j] == argv[k][j])
						return (0);
					k++;
				}
			}
			i++;
		}
		i = 0;
		j++;
	}
	return (1);
}

int		check_bloc(int argc, char **argv, int i, int j)
{
	int		i_bloc;
	int		j_bloc;

	while (i < argc - 1)
	{
		i_bloc = i;
		j_bloc = j;
		while (i <= i_bloc + 2)
		{
			if (argv[i][j] != 46)
				return (0);
			while (j <= j_bloc + 2)
			{
				if (argv[i][j] != 46)
					return (0);
				j++;
			}
			j = j_bloc;
			i++;
		}
		if (i_bloc < 6)
			i_bloc = i_bloc + 3;
		else if (i_bloc == 6 && j_bloc <= 6)
		{
			i_bloc = 0;
			j_bloc = j_bloc + 3;
		}
		i = i_bloc;
		j = j_bloc;
	}
	return (1);
}

int		check_sudoku(int argc, char **argv)
{
	int		i;
	int		j;

	if (argc == 10)
	{
		i = 0;
		j = 0;
		while (i < argc - 1)
		{
			while (argv[i][j])
			{
				if (!((argv[i][j] >= 49 && argv[i][j] <= 57) ||
					argv[i][j] == 46))
					return (0);
				j++;
			}
			if (j != 9)
				return (0);
			j = 0;
			i++;
		}
		if (check_line(argc, argv, 0, 0) == 1 &&
			check_col(argc, argv, 0, 0) == 1 && check_bloc(argc, argv, 0, 0))
			return (1);
		else
			return (0);
	}
	else
		return (0);
}
