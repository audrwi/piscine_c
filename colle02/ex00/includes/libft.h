/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarchad <pmarchad@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/16 18:11:27 by pmarchad          #+#    #+#             */
/*   Updated: 2014/08/17 23:15:27 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef	LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stdlib.h>
# include <sys/uio.h>
# include <sys/types.h>

void	ft_putchar(char c);
void	ft_putstr(char *str);
int		ft_atoi(char *str);
void	ft_putnbr(int n);
int		ft_compare(char *s1, char *s2);
void	count_line_columns(char *str);
void	calc_lines(int x, char *s, char *str, int *i);
void	colle00(int x, int y);
void	colle01(int x, int y);
void	colle02(int x, int y);
void	colle03(int x, int y);
void	colle04(int x, int y);
char	*calc_colle00(int x, int y);
char	*calc_colle01(int x, int y);
char	*calc_colle02(int x, int y);
char	*calc_colle03(int x, int y);
char	*calc_colle04(int x, int y);

#endif
