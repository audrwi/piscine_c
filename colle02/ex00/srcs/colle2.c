/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/17 20:53:21 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/17 23:25:33 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>

void	count_lines_columns(char *str, int i)
{
	int		x;
	int		y;

	while (str[i] != '\n')
		i++;
	x = i;
	i = 0;
	y = 0;
	while (str[i] != '\0')
	{
		if (str[i] == '\n')
			y++;
		i++;
	}
	if (ft_compare(str, calc_colle00(x, y)) == 1)
	{
		ft_putstr("[colle-00] [");
		ft_putnbr(x);
		ft_putstr("] [");
		ft_putnbr(y);
		ft_putstr("]\n");
	}
	else
		ft_putstr("aucune\n");
}

int		main(void)
{
	int		i;
	int		ret;
	char	buf[1];
	char	*rslt;

	ret = 1;
	i = 0;
	rslt = (char*)malloc(sizeof(buf));
	while (ret)
	{
		ret = read(0, buf, 1);
		rslt[i] = *buf;
		i++;
	}
	rslt[i] = '\0';
	count_lines_columns(rslt, 0);
	return (0);
}
