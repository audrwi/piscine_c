/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_functions.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/17 03:10:22 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/17 23:14:41 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int		i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

int		ft_atoi(char *str)
{
	int		i;

	i = 0;
	while (*str)
	{
		if (*str >= '0' && *str <= '9')
		{
			i *= 10;
			i += *str - '0';
		}
		else
			return (i);
		str++;
	}
	return (i);
}

int		ft_compare(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] || s2[i])
	{
		if (s1[i] != s2[i])
			return (1);
		i++;
	}
	return (0);
}

void	ft_putnbr(int n)
{
	int		rev_n;

	rev_n = 0;
	if (n < 0)
	{
		ft_putchar('-');
		n = -n;
	}
	while (n < 0)
	{
		rev_n *= 10;
		rev_n += n % 10;
		n /= 10;
	}
	while (rev_n > 0)
	{
		ft_putchar('0' + (rev_n % 10));
		rev_n /= 10;
	}
}
