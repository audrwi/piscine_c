/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle02.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/16 17:07:01 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/17 06:09:32 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	lines(int x, char a, char b, char c)
{
	int column;

	column = 0;
	while (column < x)
	{
		if (column == 0)
			ft_putchar(a);
		else if (column < (x - 1))
			ft_putchar(b);
		else
			ft_putchar(c);
		column++;
	}
}

void	colle02(int x, int y)
{
	int	line;

	line = 0;
	if (x >= 0 && y >= 0)
	{
		while (line < y)
		{
			if (line == 0)
				lines(x, 'A', 'B', 'A');
			else if (line < (y - 1))
				lines(x, 'B', ' ', 'B');
			else
				lines(x, 'C', 'B', 'C');
			ft_putchar('\n');
			line++;
		}
	}
}

int		main(int ac, char **av)
{
	if (ac == 3)
		colle02(ft_atoi(av[1]), ft_atoi(av[2]));
	return (0);
}
