/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc_colle00.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/17 19:43:17 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/17 22:05:09 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*calc_colle00(int x, int y)
{
	int		line;
	char	*str;
	int		i;

	str = (char*)malloc(sizeof(char) * 4096);
	i = 0;
	line = 0;
	if (x >= 0 && y >= 0)
	{
		while (line < y)
		{
			if (line == 0)
				calc_lines(x, "o-o", str, &i);
			else if (line < (y - 1))
				calc_lines(x, "| |", str, &i);
			else
				calc_lines(x, "o-o", str, &i);
			str[i] = '\n';
			i++;
			line++;
		}
		str[i] = '\0';
	}
	return (str);
}
