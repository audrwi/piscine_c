/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lines.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/17 20:07:35 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/17 22:08:21 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	calc_lines(int x, char *s, char *str, int *i)
{
	int		column;

	column = 0;
	while (column < x)
	{
		if (column == 0)
			str[(*i)] = s[0];
		else if (column < (x - 1))
			str[(*i)] = s[1];
		else
			str[(*i)] = s[2];
		column++;
		(*i)++;
	}
}
