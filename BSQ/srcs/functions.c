/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/18 21:11:58 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/20 18:54:40 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lbft.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int		i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

int		ft_atoi(char *str)
{
	int		i;

	i = 0;
	while (*str)
	{
		if (*str >= '0' && *str <= '9')
		{
			i *= 10;
			i += *str - '0';
		}
		else
			return (i);
		str++;
	}
	return (i);
}
