/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_specials.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/20 14:42:07 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/21 13:02:03 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lbft.h"

void	find_specials(char *file)
{
	int		i;
	char	*specials;

	i = 0;
	specials = (char*)malloc(sizeof(char) * 4);
	while (file[i] != '\n')
		i++;
	specials[0] = file[i - 1];
	specials[1] = file[i - 2];
	specials[2] = file[i - 3];
	specials[3] = '\0';
	i++;
	count_lines(file, i, specials);
}

void	count_lines(char *file, int i, char *specials)
{
	char	*nb_lines;
	int		nblines;
	int		lines;
	int		j;

	lines = 0;
	j = 0;
	nb_lines = (char *)malloc(sizeof(nb_lines));
	count_columns(file, i);
	while (file[i] != '\0')
	{
		if (file[i] == '\n')
			lines++;
		i++;
	}
	while (j != (i - 3))
	{
		nb_lines[j] = file[j];
		j++;
	}
	nb_lines[j] = '\0';
	nblines = ft_atoi(nb_lines);
	if (nblines != lines)
		ft_putstr("Error.\n");
	create_table(file, specials, lines, columns);
}

int		count_columns(char *file, int i)
{
	int		columns;

	columns = 0;
	while (file[i] != '\n')
	{
		columns++;
		i++;
	}
	i++;
	return (columns, i);
}
