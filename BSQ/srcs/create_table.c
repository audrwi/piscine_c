/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_table.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/20 20:25:44 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/21 12:43:46 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lbft.h"

void	create_table(char *file, char *specials, int lines, int columns)
{
	char	table[lines][columns];
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	k = 1;
	while (file[k - 1] != '\n')
		k++;
	while (i < lines)
	{
		while (j < columns)
		{
			if (file[k] != '\n')
			{
				table[i][j] = file[i];
				j++;
			}
			else
				j = 0;
			k++;
		}
		i++;
	}
}
