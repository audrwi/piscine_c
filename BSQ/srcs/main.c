/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/18 21:16:37 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/20 20:25:25 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lbft.h"

int		main(int ac, char **av)
{
	int		i;
	int		j;
	int		ret;
	char	buf[1];
	char	*file;
	int		fd;

	j = 1;
	while (j < ac && ac > 1)
	{
		i = 0;
		fd = open(av[j], O_RDONLY);
		ret = 1;
		if (fd == -1)
			ft_putstr("Open failed.\n");
		file = (char*)malloc(sizeof(buf));
		while (ret)
		{
			ret = read(fd, buf, 1);
			file[i] = *buf;
			i++;
		}
		file[i] = '\0';
		if (close(fd) == -1)
			ft_putstr("Close failed.\n");
		find_specials(file);
		j++;
	}
	return (0);
}
