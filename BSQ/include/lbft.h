/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lbft.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdevilli <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/19 17:25:25 by gdevilli          #+#    #+#             */
/*   Updated: 2014/08/20 20:30:27 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LBFT_H
# define FT_LBFT_H

# include <unistd.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>

int		ft_putchar(char c);
int		ft_putstr(char *str);
int		ft_atoi(char *str);
void	find_specials(char *file);
void	create_table(char *file, char *specials, int lines, int columns);

#endif
