/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_alphabet.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/01 02:06:08 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/01 22:17:32 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar (char c);
void	ft_print_alphabet (void);

void	ft_print_alphabet (void)
{
	char i;

	i = 'a';
	while (i <= 'z')
	{
		ft_putchar(i);
		i++;
	}
	ft_putchar('\n');
}
