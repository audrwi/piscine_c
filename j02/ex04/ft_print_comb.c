/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/01 03:00:25 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/01 22:20:57 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar (char c);
int		ft_numbers_print_comb (int a, int b, int c);
void	ft_print_comb (void);

int		ft_numbers_print_comb (int a, int b, int c)
{
	ft_putchar('0' + a);
	ft_putchar('0' + b);
	ft_putchar('0' + c);
	if (a < 7 || b < 8 || c < 9)
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void	ft_print_comb (void)
{
	char a;
	char b;
	char c;

	a = 0;
	while (a < 8)
	{
		b = a + 1;
		while (b < 9)
		{
			c = b + 1;
			while (c <= 9)
			{
				ft_numbers_print_comb(a, b, c);
				c++;
			}
			b++;
		}
		a++;
	}
	ft_putchar('\n');
}
