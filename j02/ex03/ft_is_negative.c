/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_negative.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/01 02:50:57 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/01 22:19:28 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar (char c);
void	ft_is_negative (int n);

void	ft_is_negative (int n)
{
	if (n < 0)
	{
		ft_putchar('N');
	}
	else
	{
		ft_putchar('P');
	}
	ft_putchar('\n');
}
