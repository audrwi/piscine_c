/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/01 15:28:13 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/01 22:22:11 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar (char c);
int		ft_numbers_print_comb2 (int a, int b);
void	ft_print_comb2 (void);

int		ft_numbers_print_comb2 (int a, int b)
{
	ft_putchar('0' + a / 10);
	ft_putchar('0' + a % 10);
	ft_putchar(' ');
	ft_putchar('0' + b / 10);
	ft_putchar('0' + b % 10);
	ft_putchar(',');
	ft_putchar(' ');
}

void	ft_print_comb2 (void)
{
	char a;
	char b;

	a = 0;
	while (a < 99)
	{
		b = a + 1;
		while (b <= 99)
		{
			ft_numbers_print_comb2(a, b);
			b++;
		}
		a++;
	}
}
