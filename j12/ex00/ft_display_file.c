/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/14 12:01:52 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/14 21:12:15 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_display_file.h"

#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

void	ft_display_file(char *str)
{
	int		fd;
	int		ret;
	char	buf[BUF_SIZE];

	fd = open(str, O_RDONLY);
	ret = 1;
	if (fd == -1)
		ft_putstr("Open failed.\n");
	while (ret)
	{
		ret = read(fd, buf, BUF_SIZE);
		ft_putstr(buf);
	}
	buf[ret] = '\0';
	if (close(fd) == -1)
		ft_putstr("Close failed.\n");
}
