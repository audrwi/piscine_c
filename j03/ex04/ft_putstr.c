/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/04 11:30:19 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/04 12:09:08 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putstr (char *str);

void	ft_putstr (char *str)
{
	int n;

	n = 0;
	while (str[n] != '\0')
	{
		ft_putchar(str[n]);
		n++;
	}
}
