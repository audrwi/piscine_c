/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/12 18:25:58 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/13 23:34:28 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIST
# define FT_LIST

#include <stdlib.h>
t_list	*ft_create_elem(void *data);
void	ft_list_push_back(t_list **begin_list, void *data);
typedef	struct			s_list
{
	struct	s_list		*next;
	void				*data;
}						t_list;

#endif
