/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/13 15:42:53 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/13 19:11:38 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdio.h>

int		main(void)
{
	t_list *list;

	list = ft_create_elem((void*)1337);
	if (list->next == NULL && list->data == (void*)1337)
		printf("JAYJAY");
	return (0);
}
