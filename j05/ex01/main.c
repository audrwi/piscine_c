/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/06 19:13:06 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/06 19:18:35 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strncpy (char *dest, char *src, unsigned int n);

int		main ()
{
	char dest[] = "toto";
	char src[] = "brbr";
	int n = 2;

	printf("%s%s", ft_strncpy(dest, src, n), "\n");
}
