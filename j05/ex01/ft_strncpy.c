/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/06 19:09:49 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/06 20:55:19 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncpy (char *dest, char *src, unsigned int n)
{
	int index;

	index = 0;
	while (src[index] != '\0' && index < n)
	{
		dest[index] = src[index];
		index++;
	}
	if (index = n)
	{
		dest[index] = '\0';
	}
	return (dest);
}
