/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/06 10:29:03 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/06 19:17:12 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strcpy (char *dest, char *src);

int		main ()
{
	char dest[] = "toto";
	char src[] = "brbr";
  
	printf("%s%s", ft_strcpy(dest, src), "\n");
	return (0);
}
