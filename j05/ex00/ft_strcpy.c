/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/06 10:24:46 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/07 10:47:40 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcpy (char *dest, char *src)
{
	int index;

	index = 0;
	while (src[index] != '\0')
	{
		dest[index] = src[index];
		index++;
	}
	if (src[index] == '\0')
	{
		dest[index] = '\0';
	}
	return (dest);
}
