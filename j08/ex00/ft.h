/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/08/11 11:10:25 by aboucher          #+#    #+#             */
/*   Updated: 2014/08/11 11:10:31 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H

void	ft_putchar (char c);
void	ft_putstr (char *str);
int		ft_strcmp (char *s1, char *s2);
int		ft_strlen (char *str);
void	ft_swap (int *a, int *b);

#endif
